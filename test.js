var mqtt = require('mqtt')

var client = mqtt.connect('mqtt://sonux')

client.on('connect', start)

function start () {
  console.log('subscribing')
  client.subscribe('peter/cc/hydroponics/lights')
}

client.on('message', (topic, message) => {
  console.log('got a message')
  console.log(topic, message.toString())
})
