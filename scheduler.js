var os = require('os')
var mqtt = require('mqtt')
var cron = require('cron')

var controls = {
  lights_on: 7,
  lights_off: 23,
  pump_on: [8, 12, 18]
}

var cronJobs = {}
var noop = function () {}
var retain = {
  retain: true,
  qos: 1
}

var client = mqtt.connect('mqtt://sonux', {
  clientId: os.hostname() + '_cthulu'
})

client.on('connect', start)

function start () {
  console.log('starting hydroponics controller mqtt server')

  makeFuckingSureTheLightsAreCorrect()

  // start all jobs
  cronJobs = {
    lights_on: new cron.CronJob(`0 0 ${controls.lights_on} * * *`, () => {
      client.publish('peter/cc/hydroponics/lights', 'on', retain)
    }, noop, true),
    lights_off: new cron.CronJob(`0 0 ${controls.lights_off} * * *`, () => {
      client.publish('peter/cc/hydroponics/lights', 'off', retain)
    }, noop, true),
    pump_on: new cron.CronJob(`5 0 ${controls.pump_on.join(',')} * * *`, () => {
      client.publish('peter/cc/hydroponics/pump', 'on', retain)
    }, noop, true),
    pump_off: new cron.CronJob(`35 1 ${controls.pump_on.join(',')} * * *`, () => {
      client.publish('peter/cc/hydroponics/pump', 'off', retain)

      // retry 60 seconds later just to make double sure the water turns off.
      // tho really if the first one failed, probably the second try would fail for the same reason.
      setTimeout(() => {
        client.publish('peter/cc/hydroponics/pump', 'off', retain)
      }, 60 * 1000)
    }, noop, true)
  }
}

function makeFuckingSureTheLightsAreCorrect () {
  var hour = new Date().getHours()
  var on = hour >= controls.ligths_on && hour <= controls.lights_off
  console.log('making sure that the lights are ' + (on ? 'ON' : 'OFF'))
  client.publish('peter/cc/hydroponics/lights', on ? 'on' : 'off', retain)
}
