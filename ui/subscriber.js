var mqtt = require('../mqtt/client_factory')

async function main() {
  var client = await mqtt({
    verbose: true,
    host: 'mqtt://localhost:1883',
    clientId: 'mock-ui'
  })

  client.subscribe('plnt-a/tel/temperature', (t) => {
    console.log(t, Array.prototype.slice.call(arguments))
  })

}

main().catch(console.error.bind(console))
