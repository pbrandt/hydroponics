#!/usr/bin/env bash

set -e

function ok() {
  echo -e '[ok] '"$*"
}

echo
echo INSTALLING/UPDATING DEPENDENCIES
echo
sudo apt update

