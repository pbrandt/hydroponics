#include "stdio.h"

int main() {
  double diameter_in[9] = {9.75, 11.75, 13.75, 15.5, 19.5, 19.5, 35.5, 31.0, 39.0};
  double height_in[9] = {9.0, 10.25, 11.75, 11.75, 11.75, 15.75, 14.0, 19.75, 19.75};
  double volume_gal;
  
  
  printf("%s", "GROW BAG CALCULATIONS\n");
  for (int i = 0; i < 9; i++) {
    volume_gal = diameter_in[i]*diameter_in[i]*3.14156/4.0 * height_in[i] * 0.0254 * 0.0254 * 0.0254 * 264.17205236;
    printf("Diameter %f, height %f, volume %f\n", diameter_in[i], height_in[i], volume_gal);
  }
  return 0;
}
