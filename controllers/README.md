# hydroponics

A the bottom of thisi page i have a bunch of notes from old systems. next i'd like to figure out some new systems.

here i'm going to take notes on the next system i want to build.

I think the next system is going to be highly experimental in nature. the idea is to just try to grow things because i'm curiousl, like i've never grown rice before, and i know there's a lot of water involved, and after seeing all that rice on the ssides of the road in my seoul to busan trip, i'm pretty inspired to try to grow it just to learn more about it.

the system needs a pretty good scheduler. it needs things scheuduled at various frequencies, per day, per week, month etc. and i also need to set up some sort of measuring system so that i know when the rice is ready for replanting etc.

parts of the system

- mqtt bus
- back end
    - periodic scheduler
    - measurement analysis and alerting
    - push notifications
    - video stream
- front end
    - calendar viewer / schedule editor
    - measurement viewer, long term trend analysis, probably just kibana
    - video stream viewer
    - command thing

all of this except for the calendar could be done using hive stuff. i've actually been wanting to do this for a long time in hive, but my issue is that i have too many side projects going on between marathon running and this seoul trip, and also i don't have room for a hydroponic setup in my current place.


## types of things

- lights relays
- photoresistors
- amps/watts meter
- lights housing temperature sensor (to prevent/detect fire hazards)
- water pump relays
- water pump flow meters
- solenoid valve relatys
- ph meter
- nutrient meter
- water level meter somehow
- water temperature sensors
- water bubbler relays
- fan relays
- ambient temperature sensors
- ambient humidity sensors

other things to track or somehow sense

- video cameras
- opencv processing for tracking plant height and density

---


## 2017 system

Looking at the system from the apartment side:

```
 _______________
|               |
|               |
|   4       3   |
|_______ _______|_
|\_____/ \_____/|||
|===============|||
|               |||
|               |||
|   2       1   |||
|_______ _______|||
|\_____/ \_____/|||
|===============|||
|               |--------
|               |\ pump /
|---------------| \____/

```

Switches

| name    | relay | GPIO |
| :------ | ----- | ---- |
| pump    |   1   |  21  |
| valve 1 |   7   |  19  |
| valve 2 |   6   |  16  |
| valve 3 |   5   |  26  |
| valve 4 |   4   |  20  |
| light 1 |   8   |  5   |
| light 2 |       |  6   |
| light 3 |       |  12  |
| light 4 |       |  13  |

Sensors

| name        | GPIO | Bus |
| :---------- | ---- | --- |
| pH          |      |     |
| nutrients   |      |     |
| water level |      |     |
| temperature |      |     |


## what to grow

```
┌───────────────┐
│  O    O    O  │ mesclun lettuce
│               │
│  O    O    O  │ kale
│               │
│  O    O    O  │ mesclun lettuce
│               │
│  O    X    O  │ basil
└───────────────┘

┌───────────────┐
│               │
│   O       O   │  basil
│               │
│               │
│   O       O   │  cilantro
│               │
│       X       │
└───────────────┘

┌───────────────┐
│  O    O    O  │ kale
│               │
│  O    O    O  │ mesclun lettuce
│               │
│  O    O    O  │ kale
│               │
│  O    X    O  │ parsley
└───────────────┘

┌───────────────┐
│               │
│   O       O   │ cucumber
│               │
│               │
│   O       O   │ parsley, cilantro
│               │
│       X       │
└───────────────┘
```

30 net pots total

* 9 mesclun lettuce
* 9 kale
* 2 cucumber
* 4 basil
* 3 cilantro
* 3 parsley

## high intensity system

```
┌───────────────┐
│ ═════════════ │
│               │
│ ═════════════ │
│               │
│ ═════════════ │
│               │
│  O    X    O  │
└───────────────┘
```


dimensions

```
|<--- 42 cm --->|
┌───────────────┐──
│ ═════════════ │ ^
│               │ |
│ ═════════════ │ |
│               │ 58 cm
│ ═════════════ │ |
│               │ |
│  O    X    O  │ v
└───────────────┘──


shelf

|<---------- 36 in ------------->|
┌────────────────────────────────┐──
│                                │ ^
│                                │ |
│                                │ |
│                                │
│                                │ |
│                                │ |
│                                │ v
└────────────────────────────────┘──

```


## code

crontab -e
```
@reboot env PATH=$PATH:/home/pi/n/bin pm2 start /home/pi/hydroponics.js 2>&1 | /usr/bin/logger -t hydroponics-cron
```

## old lua stuff

requires from `../lua`:

* set-time.lua
* wifi-client.lua
* relay.lua
* onthehour.lua

flashing
```
nodemcu-tool upload --optimize main.lua
nodemcu-tool upload --optimize init.lua
nodemcu-tool upload --optimize ../lua/wifi-client.lua
nodemcu-tool upload --optimize ../lua/set-time.lua
nodemcu-tool upload --optimize ../lua/relay.lua
nodemcu-tool upload --optimize ../lua/onthehour.lua
```
