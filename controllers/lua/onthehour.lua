local function debug(s)
  print(s)
end

local getTime, setNextJob, setTimeout, t

if tmr then
  debug('creating timer')
  t = tmr.create()
end

local jobs = {}

-- runs the given function f at the given hour h UTC
-- fractional hours are allowed
-- for example:
--   5:30 pm eastern standard time is UTC hour 22.5
local function onthehour(h, f)
  debug('setting up job at hour '.. h .. ' utc')
  if type(jobs[h]) == 'table' then
    table.insert(jobs[h], f)
  else
    jobs[h] = {}
    table.insert(jobs[h], f)
  end
  setNextJob()
end

-- gets the utc timestamp. then figures out what fractional hour it is right now. unusual, i know
--
getTime = function ()
  if os then
    debug('has os')
    local cal = os.date('*t', os.time())
    return cal.hour + cal.min / 60 + cal.sec / 3600
  end

  if rtctime  then
    debug('has rtctime')
    local sec, usec = rtctime.get()
    local cal = rtctime.epoch2cal(sec)
    return cal.hour + cal.min / 60 + cal.sec / 3600
  end
end

-- set an alarm for the next job to run
setNextJob = function ()
  debug('setting timer for next thing')
  local times = {}
  local maxTime = 0
  local nextTime
  local secondsToWait
  for k,v in pairs(jobs) do
    if k > maxTime then
      maxTime = k
    end
    debug(k .. ' has jobs')
    table.insert(times, k)
  end
  table.sort(times)
  
  local now = getTime()
  debug('it is now ' .. now)

  if now > maxTime then
    nextTime = times[1]
    debug('set nextTime to ' .. nextTime .. ' tomorrow')
    secondsToWait = (24 + nextTime - now) * 3600
  else
    for k,v in pairs(times) do
      if type(nextTime) ~= 'number' and v > now then
        nextTime = v
        debug('set nextTime to ' .. nextTime)
      end
    end
    secondsToWait = (nextTime - now) * 3600
  end

  debug('next job in ' .. secondsToWait .. ' seconds')

  -- if the wait is too long for tmr.alarm to handle, wait for maximum time then re-reun this
  if secondsToWait > 6870 then
    debug('delay is too long from now, will check again in a while')
    setTimeout(function ()
      setNextJob()
    end, 6870)
    return
  end

  -- schedule the next job
  setTimeout(function ()
    debug('running next job')
    for k, fn in pairs(jobs[nextTime]) do
      fn()
    end
    debug('done running next job')
    setNextJob()
  end, secondsToWait)
end

-- execute a function s seconds from now
setTimeout = function (fn, s)
  if tmr then
    -- t is created at the top of the file
    local running, mode = t:state()
    if running then
      debug('stopping old timer')
      t:stop()
      t:unregister()
    end
    
    debug('registering new timer, seconds from now: ' .. s)
    t:register(s * 1000, tmr.ALARM_SINGLE, function ()
      t:unregister()
      fn()
    end)
    t:start()
    return t
  end
end



return onthehour

