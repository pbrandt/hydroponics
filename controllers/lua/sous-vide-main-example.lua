local sensor = 4
local heater = 2
local ds18b20 = require("ds18b20")
ds18b20.setup(sensor)

function getTemp()
  local addrs = ds18b20.addrs()
  if (addrs ~= nil) then
    return ds18b20.readNumber(addrs[1], ds18b20.F)
  end
end

-- get the temperature a few times to initialize it
getTemp()
getTemp()
getTemp()
print('Temperature is '..getTemp())

-- initialize the pid controller
local pidcontroller = require('pid-controller')
local pid = pidcontroller(100)


-- set up the mqtt client
local mqttclient = mqtt.Client(HOSTNAME, 5)

-- last will and testament
mqttclient:lwt(HOSTNAME.."/tel/online", "0", 1, 1)

-- handle incoming messages, in our case just "sous-vide/cc/target" commands
mqttclient:on("message", function (client, topic, data)
  print(topic..": "..data)
  pid.setTarget(tonumber(data))
end)

local mqtt_online = false
mqttclient:connect("mqtt.peterbrandt.space", 1883, 0, function(client)
    print("connected to mqtt.peterbrandt.space")
    mqtt_online = true
    mqttclient:subscribe(HOSTNAME.."/cc/target", 1)
    mqttclient:publish(HOSTNAME.."/tel/online", "1", 1, 1)
end)

gpio.mode(heater, gpio.OUTPUT)
gpio.write(heater, 0)

tmr.alarm(1, 500, tmr.ALARM_AUTO, function ()
  local t = getTemp()
  local gain = pid.calculate(t)

  if mqtt_online then
    mqttclient:publish(HOSTNAME.."/tel/temperature", '"'..t..'"', 0, 0)
  end
end)

local control_loop_length = 2 * 1000
tmr.alarm(2, control_loop_length, tmr.ALARM_AUTO, function ()
    local gain = pid.gain
    if mqtt_online then
      mqttclient:publish(HOSTNAME.."/tel/gain", '"'..gain..'"', 0, 0)
    end

    local time_on = control_loop_length * (gain / 10)

    if (gain == 0) then
      gpio.write(heater, 0)
    elseif (gain == 10) then
      gpio.write(heater, 1)
    else
      -- on first
      gpio.write(heater, 1)
      tmr.alarm(3, time_on, tmr.ALARM_SINGLE, function ()
        gpio.write(heater, 0)
      end)
    end
end)
