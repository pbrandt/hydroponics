local relay = require('relay')
local onthehour = require('onthehour')

local pump = relay(1, 'pump')
local valve1 = relay(2, 'valve 1')
local valve2 = relay(3, 'valve 2')
local valve3 = relay(5, 'valve 3')
local valve4 = relay(6, 'valve 4')
local lights = relay(7, 'lights')
local xmaslights = relay(0, 'xmas lights')

function test()
  print('testing all the hardware')
  local dt = 2 * 1000 * 1000

  lights.on()
  pump.on()
  xmaslights.on()

  valve1.on()
  tmr.delay(dt)
  valve1.off()

  valve2.on()
  tmr.delay(dt)
  valve2.off()

  valve3.on()
  tmr.delay(dt * 2)
  valve3.off()

  valve4.on()
  tmr.delay(dt * 2)
  valve4.off()

  pump.off()
  lights.off()
  xmaslights.off()
end

local function fmod(v, den)
  return v - math.floor(v/den) * den
end

local function water(valve)
  print('watering plants')
  local floodDuration = 50 --seconds

  pump.on()

  valve.on()
  tmr.delay(floodDuration * 1000 * 1000)
  valve.off()

  pump.off()
end

-- run test
--test()

-- set up watering times
local tzoffset = 4 -- 4 in summer (EDT), 5 in winter (EST)
local basil = {6.5, 11, 15, 19}
local garlic = {7, 12, 18}

for i, t in ipairs(basil) do
  onthehour(fmod(t + tzoffset, 24), function ()
    water(valve2)
  end)
end

for i, t in ipairs(garlic) do
  onthehour(fmod(t + tzoffset, 24), function ()
    water(valve1)
  end)
end

-- set up the light system
local sunrise = fmod(7 + tzoffset, 24)
local sunset = fmod(21 + tzoffset, 24)
local currentTime = rtctime.epoch2cal(rtctime.get())
local currentHour = currentTime.hour + currentTime.min / 60 + currentTime.sec / 3600

print('currentHour:')
print(currentHour)

if currentHour >= sunrise or currentHour < sunset then
  lights.on()
end

onthehour(sunrise, lights.on)
onthehour(sunset, lights.off)

-- set up the xmas lights
local xmas_on = fmod(19 + tzoffset, 24)
local sleep = fmod(22 + tzoffset, 24)

if currentHour >= xmas_on or currentHour < sleep then
  xmaslights.on()
end

onthehour(xmas_on, xmaslights.on)
onthehour(sleep, xmaslights.off)
