
lua microcontroller code for nodemcus

should probably find a better chip with more pins

need to make instructions for flashing


probably instead of baking the wifi ssid into the lua code, should copy a file called like WIFI onto the chip and read that.


## how to set up a micro

get a custom build here: https://nodemcu-build.com/

then flash it

and use nodemcu-tool from npm

## random ideas

- [ ] make a tiny lua module which serves a single http page and writes logs over websocket
- [ ] or would it be better to write logs to a logfile or hold them in a ringbuffer and vomit them over http on request?

embedded logging is fun
