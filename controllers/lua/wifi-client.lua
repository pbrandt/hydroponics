--
-- Connect to wifi
-- this reads a file called "wifi.conf" on the device, which should have three lines: SSID, password, client-hostname
--
local function connect_wifi(callback)
  if not file.open('wifi.conf') then
    print("no wifi.conf file. should be a file with three lines: ssid, password, [this clients desired hostname]")
    return
  end
  print(file.read())
  file.seek("set", 0)
  local SSID = file.readline():gsub("\n", "")
  local PASSWORD = file.readline():gsub("\n", "")
  local HOSTNAME = file.readline():gsub("\n", "")

  -- configure wifi settings
  wifi.setmode(wifi.STATION)
  local config = {}
  config.ssid = SSID
  config.pwd = PASSWORD
  wifi.sta.config(config)

  -- repeatedly attempt to connect to wifi
  local wifi_timer = tmr.create()
  wifi_timer:alarm(1000, tmr.ALARM_AUTO, function ()
    if wifi.sta.getip() == null then
      print("connecting to " .. SSID .. " with " .. PASSWORD .. "...")
    else
      print("connected successfully to " .. SSID)
      wifi.sta.sethostname(HOSTNAME)
      print("hostname is " .. wifi.sta.gethostname())
      wifi_timer:unregister()
      callback()
    end
  end)
end

return connect_wifi
