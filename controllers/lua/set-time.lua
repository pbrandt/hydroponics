local function settime(callback)
  local timeserver = 'http://mqtt.peterbrandt.space:5000'
  http.get(timeserver, nil, function (status, time)
    print('status'..status)
    print('setting time to '..time)
    rtctime.set(tonumber(time) / 1000, (tonumber(time) % 1000) * 1000)
    callback()
  end)
end

return settime
