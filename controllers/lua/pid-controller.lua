--
-- PID controller made specifically for a system which can control in one direction only
-- in this case, it was for a DIY sous-vide which can only actively heat the water, not cool it
-- there is one input value, and one gain is calculated
-- the returned gains will be between 0 and 10, and the internal gains have been tuned for a sous-vide...
-- this is not super extensible code, just putting it here to harvest the guts later for something maybe
--
-- input:
--   target - PID target (can be changed later)
-- outputs:
--   function setTarget(target) - sets a new target
--   function calculate(value) - updates the controller with the input value "value" and returns a new gain
--



function pidcontroller(target)
  local ctl = {}
  ctl.target = target

  -- upper case i is integral error. this has a max value
  ctl.I =  0
  ctl.IMAX = 7

  -- gains are hard-coded
  kp = 1
  kd = 30
  ki = 1
  
  -- error array ring buffer
  ctl.e_length = 10
  ctl.e = {}
  for i=1, ctl.e_length do
    ctl.e[i] = 0
  end
  ctl.i = 1 -- lower case i is the current index of the measurement in the ring buffer
  ctl.gain = 0 -- gain is the output gain of the one dimensinoal controller, between 0 and 10


  ctl.calculate = function (temp)
    if (temp < ctl.target - 10) then
      ctl.gain = 10
      return 10
    end

    -- error from target temp
    local e = temp - ctl.target

    -- save error to the array, which is a ring buffer. crl.i is the current index (not integral error)
    ctl.i = ctl.i + 1
    if ctl.i > ctl.e_length + 1 then
      ctl.i = 1
    end
    ctl.e[ctl.i] = e

    -- calculate average error
    local sum = 0
    for i=1, ctl.e_length do
      sum = sum + ctl.e[i]
    end
    local avg_e = sum / ctl.e_length

    local p = e * kp
    local d = (e - avg_e) * kd
    local i = (ctl.I + avg_e) * ki

    -- sanity checks, make sure to not spin up I too much
    if (i < -ctl.IMAX) then
      i = -ctl.IMAX
    end

    -- don't even add to I if above target temp
    if (e > 0) then
      i = 0
    end

    ctl.I = i

    local gain = - p - i - d

    if (gain < 0) then
      gain = 0
    end

    if (gain > 10) then
      gain = 10
    end

    ctl.gain = gain
    return gain
  end

  -- set a new target
  ctl.setTarget = function (newtarget)
    ctl.target = newtarget
    ctl.I = 0
    ctl.gain = 0
    
    for i=1, ctl.e_length do
      ctl.e[i] = 0
    end
  end

  return ctl
end

return pidcontroller
