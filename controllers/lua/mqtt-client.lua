-- keepalive tells the server this thing is still alive so the server won't send out the last will and testament
local KEEPALIVE_TIME = 120 -- seconds
ERROR = 1
NO_ERROR = 0


--
-- makes an mqtt client with all the settings i like and convenience functions
--
local function mqtt_client(host, port, connected_callback)
    local client = {}
    client.base = wifi.sta.gethostname()

    -- create a basic mqtt client with a last will and testament to go offline
    local mc = mqtt.Client(wifi.sta.gethostname(), KEEPALIVE_TIME)
    mc:lwt(client.base .. '/tel/online', 1, 1) -- must be specified before calling connect

    mc:on("offline", function(c)
        print("mqtt client went offline")
    end)

    mc:on("message", function(c, topic, data)
        print("mqtt message | " .. topic .. " : ")
        if data ~= nil then
            print(data)
        else
            print("nil")
        end
    end)

    mc:connect(host, port, false, function(c)
        -- successful connection
        client.status = NO_ERROR
        client._mqtt_client = c

        -- setup special publish/subscribe functions
        client.telemetry = function(topic, message)
            if string.sub(topic, 1, 1) ~= "/" then
                topic = "/" .. topic
            end
            print('telemetry | ' .. topic .. ' : ' .. message)
            client._mqtt_client:publish(client.base .. '/tel' .. topic, message, 0, 0)
        end
        client.publish = function(topic, data, qos, retain, cb)
            c:publish(topic, data, qos, retain, cb)
        end
        client.subscribe = function(topic, qos, cb)
            c:subscribe(topic, qos, cb)
        end


        -- todo test a ping/pong with /sys/test

        -- send online status message
        client.publish(client.base .. '/tel/online', 1, 1, 1) -- value "1", qos 1, retain 1
        
        connected_callback(client)
    end,
    function(c, reason)
        -- failed connection
        client.status = ERROR
        client.reason = reason
        client._mqtt_client = c

        print("failed to connect to " .. host .. ":" .. port)
        print(reason)
        connected_callback(client)
    end)

end

return mqtt_client
