local last_temp
local last_humidity
function read_dhtxx(pin)
  status, temp, humi, temp_dec, humi_dec = dht.read(pin)
  if status == dht.OK then

      -- Float firmware using this example
      print("DHT Temperature:"..temp..";".."Humidity:"..humi)
      last_temp = temp
      last_humidity = humi
      return temp, humi

  elseif status == dht.ERROR_CHECKSUM then
      print( "DHT Checksum error." )
  elseif status == dht.ERROR_TIMEOUT then
      if last_temp ~= nil then
        return last_temp, last_humidity
      end
      print( "DHT timed out." )
  end
end

return read_dhtxx
