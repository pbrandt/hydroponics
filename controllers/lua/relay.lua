local ON = gpio.LOW
local OFF = gpio.HIGH

-- api to control a relay
local function relay(pin, name)
  if type(name) == 'string' then
    name = name .. ' (pin ' .. pin .. ')'
  else
    name = 'pin ' .. pin
  end

  -- relay exported api
  local r = {}

  -- turn on a relay
  r.on = function ()
    print(name .. ' on')
    gpio.write(pin, ON)
  end

  -- turn off a relay
  r.off = function ()
    print(name .. ' off')
    gpio.write(pin, OFF)
  end

  -- make sure the relay is off to start
  print('Initializing relay for ' .. name)
  gpio.mode(pin, gpio.OUTPUT)
  r.off()
  return r
end

return relay

