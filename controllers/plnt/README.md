# PLNT

temperature and humidity sensor that you can put on plants.

DIAGRAM TODO

DHT22 and some other sensor. uses a kalman filter to approximate the current temperature and humidity.

Powered by 12 volts dc. The idea is to not have 120v ac going anywhere near you and your plants. Plug in an adapter and run a 12v wire to your things.

this first thing will probably just run on an esp8266.

since it's wired (not battery) no need to mess around with sleep. you can keep it awake send data as frequently as you like.

telemetry:

```
/plnt/NUMBER/tel/online : 0 if offline and 1 if online

/plnt/NUMBER/tel/temperature : temperature in celsius output from kalman filter
/plnt/NUMBER/tel/humidity : relative humidity in % output from kalman filter

/plnt/NUMBER/tel/dht22/temperature : direct output of dht22 sensor's temperature, usually not published, unless turned on with a below command
/plnt/NUMBER/tel/dht22/humidity : direct output of dht22 sens'r humidity
/plnt/NUMBER/tel/abm123/temperature : direct output of other sensor's temp
/plnt/NUMBER/tel/asdfas/humidity : ...

/plnt/NUMBER/tel/sys/errors : string error messages from the device
/plnt/NUMBER/tel/sys/mem : memory available on device
/plnt/NUMBER/tel/sys/whatever : whatever other system metrics i can get

and for unit testing
/plnt/NUMBER/tel/sys/test : put anything here, used for testing
```

commands:
```
/plnt/NUMBER/cc/reset : reset the device
/plnt/NUMBER/cc/enable-individual-readings : if 1 then the device will publish individual readings from the sensors instead of just the output from the kalman filter
```

## Kalman filter

Inputs: [T1, T2, H1, H2]
Outputs: [T, H]

```
x_k = A x_k-1 (+ B u_k-i but we don't have a control)
z_k = H x_k

[t, h] = [1 0, 0 1] [t', h']
[t1, t2, h1, h2] = [1 0, 1 0, 0 1, 0 1] [t, h]


```

state transition matrix is linear / unit

covariance matrix... is there coupling between temperature and humidity of one device? is there coupling between temperatures of two different devices?

## Lua code

Needed modules:

- wifi-client.lua
- set-time.lua
- dhtxx.lua
- module for the other sensor
- kalman-filter.lua
- init.lua
- main.lua

also need a file called wifi.conf. remember that the esp8266 only does 2.4 band wifi, not 5.

```
SSID
PASSWORD
[desired client hostname]
```

here's some docs: https://nodemcu.readthedocs.io/en/master/

## Firmware

Need to get a custom nodemcu build from https://nodemcu-build.com/

- dht
- bme280
- file
- gpio
- http
- i2c
- mqtt
- net
- node
- spi
- tmr
- uart
- websocket
- wifi
- enable FAT (in additional build options)

to flash
https://nodemcu.readthedocs.io/en/master/flash/

1. put it in flash mode by pulling GPIO0 low. there's also a button for this on the nodemcu.
2. flash the bin from the cloud builder to 0x00000 using https://github.com/marcelstoer/nodemcu-pyflasher



## CAD files

the cad for the greenhouse was created with onshape, and you can view the file here: TODO

i want to model exactly the main building of the us botanical gardens. the two sensors are in the bottom. the nodemcu is on top in a different compartment, the reason is i don't want heat from the micro to convect upwards and affect the readings of the sensors.

## BME280

recommendations for weather station from data sheet:

```
mode: forced mode, 1 sample per minute
oversampling settings: pressure, temp, humidity 1x
IIR filter: off

```

## Configuraiton and updates

MANUAL UPDATES ONLY, not automatic OTA

if you hold the reset button, a green LED comes on and the chip is put into configuration mode. 

when in configuration mode, it is a wifi acccess point
```
SSID: plnt
password: imperio
```

then go to http://plnt.local to change the config settings

```
SSID - the name of your home network
PASSWORD - your network's password
HOSTNAME - the name of this device, defaults to "plnt"
```

you can also upload new lua files here.

maybe it would be easier to do this through configuration files and an SD card.

and if it does use an sd card, then i maybe wouldn't need mqtt either

or i could do both sd and mqtt

## TODO

- [ ] read_sensors
- [ ] make schematic
- [ ] cad
- [ ] mqtt tests
- [ ] UI
- [ ] kalman filter
