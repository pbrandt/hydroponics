// sensor fusion test

// box-muller transform
function randn_bm() {
    var u = 0, v = 0;
    while(u === 0) u = Math.random(); //Converting [0,1) to (0,1)
    while(v === 0) v = Math.random();
    return Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );
}

// DHT11
var dht11 = {
    t_sigma: 2/3,
    h_sigma: 4/3,
}

var bme280 = {
    t_sigma: 0.5/3,
    h_sigma: 3/3
}

var t = 20;
var h = 30;

function readDht11() {
    return {
        t: t + dht11.t_sigma * randn_bm(),
        h: h + dht11.h_sigma * randn_bm()
    }
}

function readBme280() {
    return {
        t: t + bme280.t_sigma * randn_bm(),
        h: h + bme280.h_sigma * randn_bm()
    }
}

var m = require('simplematrix')

// x = [t, h]
// y = [t1, h1, t2, h2]
var A = new m.Matrix([ [1, 0], [0, 1] ]) // state transition
var Q = new m.Matrix([ [0, 0], [0, 0] ]) // process noise
var H = new m.Matrix([ [1, 0], [0, 1], [1, 0], [0, 1] ]) // measurement mapping
var R = new m.Matrix([ [dht11.t_sigma, 0, 0, 0], [0, dht11.h_sigma, 0, 0], [0, 0, bme280.t_sigma, 0], [0, 0, 0, bme280.h_sigma] ]) // measurement noise

var P = new m.Matrix([ [dht11.t_sigma + bme280.t_sigma, 0], [0, dht11.h_sigma + bme280.h_sigma]]) // guess at initial covariance

// convenience
var Atranspose = A.transpose()
var Htranspose = H.transpose()

var data = {
    dht: {
        t: [],
        h: []
    },
    bme: {
        t: [],
        h: []
    },
    t: [],
    h: [],
    P: []
}

var guess = readDht11()
data.t.push(guess.t)
data.h.push(guess.h)
data.P.push(P.copy())

for (var i = 0; i < 200; i++) {
    var dht = readDht11()
    var bme = readBme280()

    // simulate a faulty sensor 10% of the time
    if (Math.random() > 0.9) {
        dht.t = 0;
        dht.h = 0;
    }

    data.dht.t.push(dht.t)
    data.dht.h.push(dht.h)
    data.bme.t.push(bme.t)
    data.bme.h.push(bme.h)

    var y = new m.Matrix([[dht.t, dht.h, bme.t, bme.h]]).transpose()
    var xprev = new m.Matrix([[data.t.slice(-1)[0], data.h.slice(-1)[0]]]).transpose()

    // prediction step
    var xpredict = A.times(xprev)
    var Ppredict = A.times(P).times(Atranspose).plus(Q)

    // update step
    var innovation = y.plus(H.times(-1).times(xpredict))
    var S = H.times(Ppredict).times(Htranspose).plus(R)
    var K = Ppredict.times(Htranspose).times(S.inverse())

    // check for faulty data
    var beta = Math.sqrt(innovation.transpose().times(S.inverse()).times(innovation)[0][0])
    if (beta > 6) {
        x = xpredict;
        P = Ppredict;
    } else {
        var x = xpredict.plus(K.times(innovation))
        P = Ppredict.plus(K.times(-1).times(S).times(K.transpose()))
    }

    // save
    data.t.push(x[0][0])
    data.h.push(x[1][0])
    data.P.push(P.copy())
}

console.log(`t is ${data.t.slice(-1)[0]}`)
console.log(`h is ${data.h.slice(-1)[0]}`)
console.log( `P is`, data.P.slice(-1)[0])
