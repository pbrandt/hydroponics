local read_dhtxx = require("dhtxx")
local mqtt_client_factory = require("mqtt-client")

local CLIENT = wifi.sta.gethostname()
local mqtt_client = nil

-- settings
local dht11_temperature_uncertainty = 2/3
local dht11_humidity_uncertainty = 4/3
local bme280_temperature_uncertainty = 0.5/3
local bme280_humidity_uncertainty = 3/3

-- variable declarations for loop use
local temperature_out = 0 -- will initialize later
local humidity_out = 0 -- will initialize later
local pressure_out = 0 -- will initialize later
local temperature_covariance_out = dht11_temperature_uncertainty + bme280_temperature_uncertainty
local humidity_covariance_out = dht11_humidity_uncertainty + bme280_humidity_uncertainty
local pressure_covariance_out = 10/3
local innovation -- used for all the things
local sigma -- used for all the things
local kalman_gain -- used for all the things
local beta -- used for all the things
local beta_stabilization_delay = 10

--
-- Read the sensors and return the raw data (kalman filter is in the main loop)
--
function read_sensors()
  local data = {}
  data.dht = {}
  data.bme = {}

  -- read dht11 or 22
  local temperature, humidity = read_dhtxx(DHT_PIN)

  if temperature == nil or humidity == nil then
    data.dht.status = ERROR
    print("read dht ERROR")
  else
    data.dht.status = NO_ERROR
    data.dht.temperature = temperature
    data.dht.humidity = humidity
    print("dht temp: " .. temperature .. ", humidity: " .. humidity)
  end

  -- read the bme280 sensor
  local temperature, pressure, humidity = bme280.read()
  if temperature == nil or pressure == nil or humidity == nil then
    print('bme error')
    data.bme.status = ERROR
  else
    temperature = temperature / 100
    humidity = humidity / 1000
    pressure = pressure / 1000
    print("bme temp: " .. temperature, ", humidity: " .. humidity .. ", pressure: " .. pressure)
    data.bme.temperature = temperature
    data.bme.humidity = humidity
    data.bme.pressure = pressure
    data.bme.status = NO_ERROR
  end

  return data
end

--
-- self-test function which should run at each start-up
-- 1. Runs hardware checks
-- 2. Runs software checks
-- 3. if the test fails, then the red LED is lit, if it passes, the green one is lit
--
function initialize(cb)
  print('setting up i2c bus and bm280')
  i2c.setup(0, I2C_SDA, I2C_SCL, i2c.SLOW)
  bme280.setup()

  print('running hardware self test')
  local status = NO_ERROR

  -- 
  -- HARDWARE TESTS
  --

  -- try to read a few times
  local data = read_sensors()
  data = read_sensors()
  data = read_sensors()
  data = read_sensors()
  data = read_sensors()

  -- if the sensor read ERROR, log an error and continue
  if data.status == ERROR then
    -- mqtt_client.telemetry(CLIENT..'/tel/errors', 'ERROR to read sensor')
    status = ERROR
  else
    -- initialize filter with current readings
    temperature_out = data.bme.temperature
    humidity_out = data.bme.humidity
    pressure_out = data.bme.pressure
  end

  --
  -- ASYNC SOFTWARE TESTS
  --
  mqtt_client_factory('192.168.50.127', 1883, function(client)
    if (client.status == ERROR) then
      print("mqtt error: " .. client.reason)
      status = ERROR
    else
      print("connected to mqtt status" .. client.status)
      mqtt_client = client
    end

    cb(NO_ERROR)
  end)

end

--
-- Main loop: runs every 1000 milliseconds
-- 1. read sensors
-- 2. publish data
-- 3. update kalman filter
-- 4. publish kalman filter state
--
function nominal_operation()
  --local kf = kalman_filter(covariance, stm, 2, 4)

  local main_timer = tmr.create()
  main_timer:alarm(1000, tmr.ALARM_AUTO, function ()
    -- prediction step
    -- nothing to do here since A is identity and Q is zero

    -- read sensors and send telemetry of the raw values
    local data = read_sensors()
    mqtt_client.telemetry('/dht11/temperature', data.dht.temperature)
    mqtt_client.telemetry('/dht11/humidity', data.dht.humidity)
    mqtt_client.telemetry('/bme280/temperature', data.bme.temperature)
    mqtt_client.telemetry('/bme280/humidity', data.bme.humidity)

    -- get closer to stable
    if beta_stabilization_delay > 0 then
      beta_stabilization_delay = beta_stabilization_delay - 1
    end

    -- update temperature with DHT data
    innovation = data.dht.temperature - temperature_out
    sigma = temperature_covariance_out + dht11_temperature_uncertainty
    kalman_gain = temperature_covariance_out / sigma
    beta = innovation * innovation / sigma -- actually beta squared
    print("y: " .. innovation .. " s: " .. sigma .. " k: " .. kalman_gain)

    -- if the error is greater than 6 sigma, it's an error
    if beta > 6 * 6 and beta_stabilization_delay == 0 then
      -- send error
      print("dht t beta is: "..beta)
    else
      temperature_out = temperature_out + kalman_gain * innovation
    end

    -- update humidity out with DHT data
    innovation = data.dht.humidity - humidity_out
    sigma = humidity_covariance_out + dht11_humidity_uncertainty
    kalman_gain = humidity_covariance_out / sigma
    beta = innovation * innovation / sigma -- actually beta squared
    print("y: " .. innovation .. " s: " .. sigma .. " k: " .. kalman_gain)

    -- if the error is greater than 6 sigma, it's an error
    if beta > 6 * 6 and beta_stabilization_delay == 0 then
      -- send error
      print("dht h beta is: "..beta)
    else
      humidity_out = humidity_out + kalman_gain * innovation
    end

    -- update temperature with bme data
    innovation =  data.bme.temperature - temperature_out
    sigma = temperature_covariance_out + bme280_temperature_uncertainty
    kalman_gain = temperature_covariance_out / sigma
    beta = innovation * innovation / sigma -- actually beta squared
    print("y: " .. innovation .. " s: " .. sigma .. " k: " .. kalman_gain)

    -- if the error is greater than 6 sigma, it's an error
    if beta > 6 * 6 and beta_stabilization_delay == 0 then
      -- send error
      print("bme t beta is: "..beta)
    else
      temperature_out = temperature_out + kalman_gain * innovation
    end

    -- update humidity out with bme data
    innovation = data.bme.humidity - humidity_out
    sigma = humidity_covariance_out + bme280_humidity_uncertainty
    kalman_gain = humidity_covariance_out / sigma
    beta = innovation * innovation / sigma -- actually beta squared
    print("y: " .. innovation .. " s: " .. sigma .. " k: " .. kalman_gain)

    -- if the error is greater than 6 sigma, it's an error
    if beta > 6 * 6 and beta_stabilization_delay == 0 then
      -- send error
      print("bme h beta is: "..beta)
    else
      humidity_out = humidity_out + kalman_gain * innovation
    end
    
    
    -- send out the kalm filter outputs
    mqtt_client.telemetry('/temperature', temperature_out)
    mqtt_client.telemetry('/humidity', humidity_out)
  end)
end

-- start it
initialize(function(status)
  if status == NO_ERROR then
    print("passed self checks")
    nominal_operation()
  end
end)
