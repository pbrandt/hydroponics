var mqtt = require('../../mqtt/client_factory.js')

// name this sensor
const name = process.argv[2] || 'a'
const clientString = 'plnt-a';
console.log(name)

// box-muller transform
function randn_bm() {
  var u = 0, v = 0;
  while(u === 0) u = Math.random(); //Converting [0,1) to (0,1)
  while(v === 0) v = Math.random();
  return Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );
}

// random variable 

var currentTemp = (67 - 32) * 5 / 9;
function randomTemp() {
  return currentTemp + 2 * ( Math.random() - 0.5);
}

var currentHumidity = 35;
function randomHumidity() {
  return currentHumidity + 5 * (Math.random() - 0.5);
}

async function main() {
  const client = await mqtt({
    verbose: true,
    host: 'mqtt://localhost:1883',
    clientId: clientString
  });

  setInterval(() => {
    client.publish(clientString + '/tel/temperature', randomTemp());
    client.publish(clientString + '/tel/humidity', randomHumidity());
  }, 1000);
}

main().catch(console.error.bind(console))

