#!/usr/bin/env bash

set -e

# PLNT directory, where this script is. maybe i should use git to find the root dir
DIR="$(cd "$(dirname ${BASH_SOURCE[0]})" && pwd)"

# lua directory
LUA="$(dirname "$DIR")"/lua

echo attempting to flash the PLNT controller code

# this function just uploads files for now
# hopefully i'll figure out a way to know if a file is up to date or not
function syncfile() {
  echo syncing $1
  # check timestamps?

  # upload to device
  nodemcu-tool upload $1
}

syncfile $LUA/wifi-client.lua
syncfile $LUA/set-time.lua
syncfile $LUA/dhtxx.lua
syncfile $LUA/kalman-filter.lua

syncfile $DIR/init.lua
syncfile $DIR/main.lua
syncfile $DIR/wifi.conf

