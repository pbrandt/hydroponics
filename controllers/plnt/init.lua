-- pin definitions
-- gpio.mode(0, gpio.INPUT) -- D0 4 GPIO16 DO NOT USE. only read/write. it's the flash one.
gpio.mode(1, gpio.INPUT) -- D1 20 GPIO5
SAFEBOOT_PIN = 1
gpio.mode(2, gpio.INPUT) -- D2 19 GPIO4 PWM
DHT_PIN = 2
-- gpio.mode(3, gpio.OPENDRAIN) -- D3 18 GPIO0 SPI_CS2
I2C_SDA = 3
-- gpio.mode(4, gpio.OPENDRAIN) -- D4 17 GPIO2 U1TXD
I2C_SCL = 4
-- gpio.mode(5, gpio.OPENDRAIN) -- D5 5 GPIO14 PWM, MTMS, NSPI CLK
-- gpio.mode(6, gpio.OPENDRAIN) -- D6 6 GPIO12 PWM, MTDI, NSPI MISO
-- gpio.mode(7, gpio.OPENDRAIN) -- D7 7 GPIO13 MTCK, U0CTS, NSPI MOSI
-- gpio.mode(8, gpio.OPENDRAIN) -- D8 16 GPIO15 PWM, MTDO, U0RTS, NSPI CS
-- gpio.mode(9, gpio.OPENDRAIN) -- RX 21 GPIO3 U0RXD
-- gpio.mode(10, gpio.OPENDRAIN) -- TX 22 GPIO1 U0TXD, SPI_CS1
-- gpio.mode(11, gpio.OPENDRAIN) -- SD2 11 GPIO9 SDIO DATA2, SPIHD, HSPIHD
-- gpio.mode(12, gpio.OPENDRAIN) -- SD3 12 GPIO10 SDIO DATA3, SPIWP HSPIWP

-- above are from documentation. bunch of pins left still:
-- A0 2 ADC TOUT
-- SD1 13 GPIO8 SDIO DATA1, SPI_MOSI, U1RXD
-- CMD 9 GPIO11 SDIO CMD, SPI_CS0
-- SD0 10 GPIO7 SDIO DATA0, SPI_MISO
-- CLK 14 GPIO6 SDIO CLK, SPI_CLK
-- EN 1 EXT_RSTB
-- RST 3 CHIP_EN, CH_PD (are these mixed up / labelled wrong?)


-- common definitions
ERROR = 1
NO_ERROR = 0


-- D1 is a safety pin that prevents crashes/restarts by not executing any logic
-- set HIGH for safeboot, ground for normal operation
if gpio.read(SAFEBOOT_PIN) == gpio.HIGH then
  print('safeboot pin is high - not doing anything')
  print('set pin D1 to ground to return to normal operations')
else
  local wificlient = require('wifi-client')
  local settime = require('set-time')

  --
  -- Broad setup of the device when it's powered on:
  -- 1. set up wifi
  -- 2. set the time from the internet
  -- 3. run the specific logic for this thing
  --
  wificlient(function()
    print("connected to wifi")
    dofile("main.lua")
  end)
end
