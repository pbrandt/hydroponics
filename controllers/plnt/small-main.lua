local read_dhtxx = require("dhtxx")
local mqtt_client_factory = require("mqtt-client")


local GREEN_LED = 5
local RED_LED = 6
local OKAY = 1
local FAILED = 0
local DHT_PIN = 2
local OTHER_PIN = 0

local CLIENT = 'plnt-a'
local mqtt_client = nil




function read_sensors()
  local data = {}
  data.dht = {}
  data.bme = {}
  data.okay = OKAY

  
  local temperature, humidity = read_dhtxx(DHT_PIN)

  if temperature == nil or humidity == nil then
    data.okay = FAILED
    print("read dht failed")
  else
    data.dht.temperature = temperature
    data.dht.humidity = humidity
    print("dht temp: " .. temperature .. ", humidity: " .. humidity)
  end

  



  return data
end







function initialize(cb)
  print('running hardware self test')
  local status = OKAY

  
  
  

  
  local data = read_sensors()
  data = read_sensors()
  data = read_sensors()
  data = read_sensors()
  data = read_sensors()

  
  if data.okay == FAILED then
    
    status = FAILED
  end

  
  
  
  
  

  mqtt_client_factory('192.168.50.127', 1883, function(client)
    if client.okay == OKAY then
      mqtt_client = client
    else
      status = FAILED
    end

    if status == OKAY then
      print("ok green led")
      
      
    else
      print("not ok red led")
      
      
    end

    cb(OKAY)
  end)

end








function nominal_operation()
  

  local main_timer = tmr.create()
  main_timer:alarm(1000, tmr.ALARM_AUTO, function ()
    local data = read_sensors()
    if data.okay == FAILED then
      return
    end

    mqtt_client.telemetry('/dht/temperature', data.dht.temperature)
    mqtt_client.telemetry('/dht/humidity', data.dht.humidity)
    
    
    
    local vec = {}
    vec[0] = data.dht.temperature
    vec[1] = data.other.temperature
    vec[2] = data.dht.humidity
    vec[3] = data.other.humidity
    
    
    
  end)
end


initialize(function(status)
  if status == OKAY then
    print("passed self checks")
    nominal_operation()
  end
end)
