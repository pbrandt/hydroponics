#include <ESP8266WiFi.h>
#include <AsyncMqttClient.h>

// YEAH
// HYROPONICS
// WOOO

AsyncMqttClient mqttClient;

const char* ssid = "LES";
const char* password = "huevosdiablos";
const char* mqtturl = "mqtt://192.168.1.210";

const char* lights = "peter/cc/hydroponics/lights";
const char* pump = "peter/cc/hydroponics/pump";
const char* online = "peter/tel/hydroponics/online";

int LIGHTS_PIN = D1;
int PUMP_PIN = D2;
int ON = LOW;
int OFF = HIGH;

void onMqttConnect() {
  Serial.println("** Connected to the broker **");
  uint16_t packetIdSub = mqttClient.subscribe(lights, 1);
  Serial.print("Subscribing to light commands at QoS 1, packetId: ");
  Serial.println(packetIdSub);

  packetIdSub = mqttClient.subscribe(pump, 1);
  Serial.print("Subscribing to water pump commands at QoS 1, packetId: ");
  Serial.println(packetIdSub);
  
  mqttClient.publish(online, 1, true, "yes");
  Serial.println("Hydroponics system online");
}

void onMqttDisconnect(AsyncMqttClientDisconnectReason reason) {
  Serial.println("** Disconnected from the broker **");
  Serial.println("Reconnecting to MQTT...");
  mqttClient.connect();
}

void onMqttSubscribe(uint16_t packetId, uint8_t qos) {
  Serial.println("** Subscribe acknowledged **");
  Serial.print("  packetId: ");
  Serial.println(packetId);
  Serial.print("  qos: ");
  Serial.println(qos);
}

void onMqttUnsubscribe(uint16_t packetId) {
  Serial.println("** Unsubscribe acknowledged **");
  Serial.print("  packetId: ");
  Serial.println(packetId);
}

void onMqttMessage(char* topic, char* payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total) {
  Serial.println("** Publish received **");
  Serial.print("  topic: ");
  Serial.println(topic);
  Serial.print("  qos: ");
  Serial.println(properties.qos);
  Serial.print("  dup: ");
  Serial.println(properties.dup);
  Serial.print("  retain: ");
  Serial.println(properties.retain);
  Serial.print("  len: ");
  Serial.println(len);
  Serial.print("  index: ");
  Serial.println(index);
  Serial.print("  total: ");
  Serial.println(total);
  Serial.print("  payload: ");
  Serial.println(payload);

  if (strcmp(topic, lights) == 0) {
    if (strcmp(payload, "on") == 0) {
      digitalWrite(LIGHTS_PIN, ON);
    } else {
      digitalWrite(LIGHTS_PIN, OFF);
    }
  }

  if (strcmp(topic, pump) == 0) {
    if (strcmp(payload, "on") == 0) {
      digitalWrite(PUMP_PIN, ON);
    } else {
      digitalWrite(PUMP_PIN, OFF);
    }
  }

  
}

void onMqttPublish(uint16_t packetId) {
  Serial.println("** Publish acknowledged **");
  Serial.print("  packetId: ");
  Serial.println(packetId);
}

void setup() {
  Serial.begin(115200);
  Serial.println();
  Serial.println();
  WiFi.persistent(false);
  WiFi.mode(WIFI_STA);
  Serial.print("Connecting to Wi-Fi");
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  mqttClient.onConnect(onMqttConnect);
  mqttClient.onDisconnect(onMqttDisconnect);
  mqttClient.onSubscribe(onMqttSubscribe);
  mqttClient.onUnsubscribe(onMqttUnsubscribe);
  mqttClient.onMessage(onMqttMessage);
  mqttClient.onPublish(onMqttPublish);
  mqttClient.setServer(IPAddress(192, 168, 1, 210), 1883);
//  mqttClient.setKeepAlive(5).setWill("topic/online", 2, true, "no").setCredentials("username", "password").setClientId("myDevice");
  mqttClient.setKeepAlive(5).setWill("peter/tel/hydroponics/online", 1, true, "no").setClientId("hydroponics");
  Serial.println("Connecting to MQTT...");
  mqttClient.connect();

  pinMode(LIGHTS_PIN, OUTPUT);
  pinMode(PUMP_PIN, OUTPUT);
  digitalWrite(LIGHTS_PIN, OFF);
  digitalWrite(PUMP_PIN, OFF);
}

void loop() {
}
