const Pin = require('onoff').Gpio
const RELAY_ON = 0
const RELAY_OFF = 1
class Relay {
  constructor(n, name) {
    this.n = n
    this.name = typeof name === 'string' ? `${name} (pin ${n})` : `pin ${n}`
    console.log(`initializing relay for ${this.name}`)
    try {
      this.pin = new Pin(n, 'out')
    } catch (e) {
      this.pin = {
        writeSync: function () {}
      }
    }
    this.state = 'off'
  }

  get state() {
    return this._state
  }

  set state(value) {
    this._state = value
    if (value === 'on') {
      this.pin.writeSync(RELAY_ON)
    } else if (value === 'off') {
      this.pin.writeSync(RELAY_OFF)
    }
  }

  on() {
    console.log(`${this.name} on`)
    this.state = 'on'
  }

  off() {
    console.log(`${this.name} off`)
    this.state = 'off'
  }

  toggle() {
    this.state = this.state === 'on' ? 'off' : 'on'
    console.log(`toggling ${this.name} ${this.state}`)
  }
}

//
// Set up IO
//
const pump = new Relay(21, 'pump')
const valve = [19, 16, 26, 20].reduce((valve, pin, i) => {
  valve[i+1] = new Relay(pin, 'valve ' + (i+1))
  return valve
}, {})
const light = [5, 6, 12, 13].reduce((light, pin, i) => {
  light[i+1] = new Relay(pin, 'light ' + (i+1))
  return light
}, {})

//
// Set up for a schedule in UTC
//
const schedule = {
  lights: {
    on: 11,
    off: 1
  },
  water: [
    [ 11, 13, 15, 17, 19, 21, 23, 2, 7]
  ]
}

const cron = require('node-cron')
var jobs = []
function setup(schedule) {
  // first stop any existing jobs before restarting them
  jobs.map(j => j.stop())
  jobs = []

  // get the UTC hour
  var date = new Date()
  var hour = date.getUTCHours() + date.getUTCMinutes()/60

  // Lights (only one for now)
  var light_job = cron.schedule(`0 0 ${schedule.lights.on} * * *`, () => {
    light[1].on()
  })
  jobs.push(light_job)

  light_job = cron.schedule(`0 0 ${schedule.lights.off} * * *`, () => {
    light[1].off()
  })
  jobs.push(light_job)

  if (hour > schedule.lights.on || hour < schedule.lights.off) {
    light[1].on()
  } else {
    light[1].off()
  }

  // Valves (all of them)
  schedule.water.map((times, i) => {
    i++
    var water_job = cron.schedule(`0 0 ${times.join(',')} * * *`, async () => {
      pump.on()
      valve[i].on()
      await wait(60)
      pump.off()
      valve[i].off()
    })
    jobs.push(water_job)
  })
}

function wait(seconds) {
  return new Promise((r) => setTimeout(r, seconds * 1000))
}

setup(schedule)
