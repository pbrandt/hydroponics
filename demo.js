var mqtt = require('mqtt')

var client = mqtt.connect('mqtt://sonux')
console.log('fancy demo making things work in wondrous awe')

client.on('connect', start)

var lights = 'peter/cc/hydroponics/lights'
var pump = 'peter/cc/hydroponics/pump'

function start () {
  client.subscribe('peter/cc/hydroponics/lights')
}

var init = false
client.on('message', (topic, message) => {
  console.log('got a message')
  console.log(topic, message.toString())
  if (!init) {
    init = true;
    sec(lights, 'off')
    sec(lights, 'on')
    sec(lights, message)
    sec(pump, 'on')
    sec(pump, 'off')
    run()
  }
})

var q = []
function sec (topic, value) {
  q.push({topic: topic, value: value})
}

function run () {
  setInterval(function () {
    if (q.length === 0) process.exit()
    var d = q.shift()
    console.log(d)
    client.publish(d.topic, d.value)
  }, 4000)
}
