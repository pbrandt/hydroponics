# hydroponics

research and code related to hydroponics for smaller indoor spaces.

MQTT is used for commands and telemetry to and from the equipment. The equipment is not "smart", meaning it is supposed to just listen for commands froma central server. However, the equipment should definitely have safety controls, fail to a safe state, be able to reject unsafe commands, and provide way more telemetry than you think is needed to troubleshoot a problem.

## System Diagram

![System Diagram](system-diagram.svg)


## Data

MQTT topics take the following format:

```
DEVICE_TYPE/[NAME (OPTIONAL)]/[PROCESS (OPTIONAL)]/tel/[HARDWARE]/DATAPOINT
DEVICE_TYPE/[NAME (OPTIONAL)]/[PROCESS (OPTIONAL)]/cc/[HARDWARE]/COMMAND
```

A micro probably controls many of each thing, like `.../lights/0` through `.../lights/3`. To subscribe to all data for all lights on microcontroller "b":

```
controller/b/tel/lights/*
```

To turn on the first set of lights it controls

```
mqtt.send({
    topic: 'controller/b/cc/lights/0/on',
    data: 1
})
```

## Topics

lights need to have temperature sensors because they get hot. current and power sensors would be good too. pwm control for the lights. water levels, ph, nutrient levels, 

video streams are another thing, probably not mqtt


## Running

On the mainframe, clone this repo and do `./mainframe-install.sh` and `pm2 start mainframe-ecosystem.json`

for the microcontrollers, flash them with nodemcu-tool? tbd 

