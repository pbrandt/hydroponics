const clientFactory = require('./client_factory')

async function main() {
  const client = await clientFactory({
    verbose: true,
    host: 'mqtt://localhost:1883',
    clientId: 'tester1'
  })

  console.log('subscribing to test/temp')
  await client.subscribe('test/temp', (t) => {
    console.log('client 1 got temp', t)
  })
  console.log('subscription successful')

  console.log('publishing 100...')
  await client.publish('test/temp', 100)
  console.log('publishing 200...')
  await client.publish('test/temp', 200)

  //
  // SINE EXAMPLE
  //
  const meta = {
    display_name: 'Example Sine',
    upper_limit: 0.9
  }

  client.publish('/example/sine/meta', meta, {
    qos: 2,
    retain: 1
  })

  var t = 0;
  setInterval(() => {
    client.publish('/example/sine', Math.sin(t));
    t = t + 0.1;
  }, 1000);

}

main().catch(console.error.bind(console));
