const mosca = require('mosca')
require('colors')

var ascoltatore = {
  type: 'redis',
  redis: require('redis'),
  db: 12,
  port: 6379,
  return_buffers: true, // to handle binary payloads
  host: "localhost"
};

var settings = {
  port: 1883,
  backend: ascoltatore,
  persistence: {
    factory: mosca.persistence.Redis
  },
  http: {
    port: 8883
  }
};


async function main() {
  console.dir(settings)
  // const db = await require('../db')
  var server = new mosca.Server(settings)

  server.on('clientConnected', function (client) {
    console.log('client connected', client.id)
  })

  // fired when a message is received
  server.on('published', function (packet, client) {
    if (!client) {
      client = {id: 'mqtt-server'}
    }

    var doc = {
      client: client.id,
      topic: packet.topic,
      data: packet.payload.toString(),
      qos: packet.qos || 0,
      retain: packet.retain || false,
      ts: +new Date()
    }

    var docstring = doc.topic.cyan + ' > '  + doc.data.yellow + ` (q${doc.qos} ${doc.retain ? '' : 'not '}retained) from ${doc.client.green}`

    if (doc.topic.match(/^\$SYS/)) {
      docstring = docstring.reset.grey
    } else {
      console.log(docstring)
    }

    // db.insert('mqtt_tbl', doc)

  })

  server.on('ready', setup)

  function setup () {
    //server.authenticate = auth.authenticate
    //server.authorizePublish = auth.authorizePublish
    //server.authorizeSubscribe = auth.authorizeSubscribe
    console.log('Mosca server is up and running')
  }
}

main().catch(console.error.bind(console))
