const clientFactory = require('./client_factory')

async function main() {
  const client = await clientFactory({
    verbose: true,
    host: 'mqtt://localhost:1883',
    clientId: 'tester1'
  })

  console.log('subscribing to test/temp')
  await client.subscribe('test/temp', (t) => {
    console.log('client 1 got temp', t)
  })
  console.log('subscription successful')

  console.log('publishing 100...')
  await client.publish('test/temp', 100)
  console.log('publishing 200...')
  await client.publish('test/temp', 200)


}

main().catch(console.error.bind(console));
