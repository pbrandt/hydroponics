// set an alarm for an mqtt topic
require('colors')

var client

module.exports = async function(topic, condition) {
  if (!client) {
    client = await require('./client_factory')({
      clientId: 'alarm' + Math.random().toString().slice(5)
    })
  }

  console.log(`[alarm] ${topic} ${condition.toString()}`.gray)

  return new Promise(resolve => {
    var resolved = false;
    client.subscribe(topic, d => {
      if (condition(d) && !resolved) {
        resolved = true;
        resolve(d)
      }
    })
  })
}

if (!module.parent) {
  const topic = process.argv[2]
  const value = parseFloat(process.argv[3])
  console.log(`Waiting for ${topic} to hit ${value}`)
}