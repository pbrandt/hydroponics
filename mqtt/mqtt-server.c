
/*
 * Copyright (c) 2014 Cesanta Software Limited
 * All rights reserved
 * This software is dual-licensed: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation. For the terms of this
 * license, see <http://www.gnu.org/licenses/>.
 *
 * You are free to use this software under the terms of the GNU General
 * Public License, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * Alternatively, you can license this software under a commercial
 * license, as set out in <https://www.cesanta.com/license>.
 */


// mongoose is an embeddable networking library
#define MG_ENABLE_MQTT_BROKER 1
#include "mongoose.h"
#include "mongoose.c"

// #include "memory.h"
// #include "string.h"
// #include "sqlite3.h"
// #include "sqlite3.c"

// database

static const char * s_mqtt_address = "0.0.0.0:1883";
static const char * s_websocket_address = "0.0.0.0:8883";

static void log_to_db(struct mg_connection * c, int ev, void * ev_data) {
    printf("%s\n", "logging to database");
}

// un-proxy a websocket to mqtt connection
static void unproxy(struct mg_connection * c) {
  struct mg_connection * websocket_connection = (struct mg_connection *) c->user_data;
  if (websocket_connection != NULL) {
    websocket_connection->flags |= MG_F_CLOSE_IMMEDIATELY;
    websocket_connection->user_data = NULL;
    c->user_data = NULL;
  }
  printf("Closing connection %p\n", c);
}

//
// Proxy stuff from websocket to mqtt socket
// this function is the event handler from the MQTT server that goes back to websocket
//
static void proxy_handler(struct mg_connection * c, int ev, void * ev_data) {
  if (ev == MG_EV_POLL) {
    return; // shouldn't this be handled elsewhere? idk
  }
  if (ev == MG_EV_CLOSE) {
    // R I P
    unproxy(c);
  } else if (ev == MG_EV_RECV) {
    struct mg_connection * websocket_connection = (struct mg_connection *) c->user_data;
    if (websocket_connection != NULL) {
      mg_send_websocket_frame(websocket_connection, WEBSOCKET_OP_BINARY, c->recv_mbuf.buf, c->recv_mbuf.len);
    }
  }
}



/**
 * handles mqtt events
 */
static void custom_mqtt_handler(struct mg_connection * c, int ev, void * ev_data) {
  // do specific stuff for the event like save to DB or something
  if (ev == MG_EV_POLL) {
      // do nothing, nothing to log to the db
      return;
  } else if (ev < MG_MQTT_EVENT_BASE) {
    if (ev == MG_MQTT_CMD_CONNECT) {
        printf("event %d : %s\n", ev, "MG_MQTT_CMD_CONNECT");
    } else if (ev == MG_MQTT_CMD_CONNACK) {
        printf("event %d : %s\n", ev, "MG_MQTT_CMD_CONNACK this is highly unexpected");
    } else if (ev == MG_MQTT_CMD_PUBLISH) {
        printf("event %d : %s\n", ev, "MG_MQTT_CMD_PUBLISH");
    } else if (ev == MG_MQTT_CMD_PUBACK) {
        printf("event %d : %s\n", ev, "MG_MQTT_CMD_PUBACK");
    } else if (ev == MG_MQTT_CMD_PUBREC) {
        printf("event %d : %s\n", ev, "MG_MQTT_CMD_PUBREC");
    } else if (ev == MG_MQTT_CMD_SUBSCRIBE) {
        printf("event %d : %s\n", ev, "MG_MQTT_CMD_PUBLISH");
    } else {
        printf("Got unhandled mqtt event %d\n", ev);
    }
  } else if (ev > MG_MQTT_EVENT_BASE) {
      if (ev == MG_EV_MQTT_CONNECT) {
        printf("event %d : %s\n", ev, "MG_EV_MQTT_CONNECT");
      } else if (ev == MG_EV_MQTT_CONNACK) {
        printf("event %d : %s\n", ev, "MG_EV_MQTT_CONNACK");
      } else if (ev == MG_EV_MQTT_PUBLISH) {
        printf("event %d : %s\n", ev, "MG_EV_MQTT_PUBLISH");
      } else if (ev == MG_EV_MQTT_PUBACK) {
        printf("event %d : %s\n", ev, "MG_EV_MQTT_PUBACK");
      } else if (ev == MG_EV_MQTT_PUBREC) {
        printf("event %d : %s\n", ev, "MG_EV_MQTT_PUBREC");
      } else if (ev == MG_EV_MQTT_PUBREL) {
        printf("event %d : %s\n", ev, "MG_EV_MQTT_PUBREL");
      } else if (ev == MG_EV_MQTT_PUBCOMP) {
        printf("event %d : %s\n", ev, "MG_EV_MQTT_PUBCOMP");
      } else if (ev == MG_EV_MQTT_SUBSCRIBE) {
        printf("event %d : %s\n", ev, "MG_EV_MQTT_SUBSCRIBE");
      } else if (ev == MG_EV_MQTT_SUBACK) {
        printf("event %d : %s\n", ev, "MG_EV_MQTT_SUBACK");
      } else if (ev == MG_EV_MQTT_UNSUBSCRIBE) {
        printf("event %d : %s\n", ev, "MG_EV_MQTT_UNSUBSCRIBE");
      } else if (ev == MG_EV_MQTT_UNSUBACK) {
        printf("event %d : %s\n", ev, "MG_EV_MQTT_UNSUBACK");
      } else if (ev == MG_EV_MQTT_PINGREQ) {
        printf("event %d : %s\n", ev, "MG_EV_MQTT_PINGREQ");
        // send a pong back
        // mg_mqtt_pong(c);
      } else if (ev == MG_EV_MQTT_PINGRESP) {
        printf("event %d : %s\n", ev, "MG_EV_MQTT_PINGRESP");
      } else if (ev == MG_EV_MQTT_DISCONNECT) {
        printf("event %d : %s\n", ev, "MG_EV_MQTT_DISCONNECT");
      } else {
        printf("Got unhandled mqtt event %d\n", ev);
      }
  }

  // call the broker service i guess
  mg_mqtt_broker(c, ev, ev_data);
}

static void custom_websocket_handler(struct mg_connection *c, int ev, void *ev_data) {
  if (ev == MG_EV_POLL) {
    return;
  }

  printf("custom websocket handler%s\n", "");

  struct mg_connection *mqtt_connection = (struct mg_connection *) c->user_data;

  // something something custom here maybe auth

  if (ev == MG_EV_WEBSOCKET_HANDSHAKE_DONE) {
    // proxy it to the mqtt socket
    // wonder if i can do this without sockets, like directly hook up to event handler
    mqtt_connection = mg_connect(c->mgr, s_mqtt_address, proxy_handler);
    mqtt_connection->user_data = c; // store incoming connection
    c->user_data = mqtt_connection; // so we can retrieve the MQTT connection 
    printf("Created proxy connection from websocket to mqtt socket %p\n", mqtt_connection);
  } else if (ev == MG_EV_WEBSOCKET_FRAME) {
    struct websocket_message *message = (struct websocket_message *) ev_data;
    if (mqtt_connection != NULL) {
      printf("Forwarding %d bytes to mqtt server from websocket\n", (int)message->size);
      mg_send(mqtt_connection, message->data, message->size);
    }
  } else if (ev == MG_EV_CLOSE) {
    // R I P
    unproxy(c);
  }
}

int main(void) {
  // connection manager
  struct mg_mgr mgr;
  mg_mgr_init(&mgr, NULL);

  //
  // MQTT listener network connection
  //
  struct mg_connection *mqtt_listener;
  struct mg_mqtt_broker broker;
  mqtt_listener = mg_bind(&mgr, s_mqtt_address, custom_mqtt_handler);

  // check for errors
  if (mqtt_listener == NULL) {
    fprintf(stderr, "mqtt mg_bind(%s) failed\n", s_mqtt_address);
    exit(EXIT_FAILURE);
  }

  // do whatever initialization need to be done
  mg_mqtt_broker_init(&broker, NULL);

  // store a reference to the broker in the user_data field because humans are descended from packrats
  mqtt_listener->user_data = &broker;

  // yolo
  mg_set_protocol_mqtt(mqtt_listener);
  printf("MQTT broker started on mqtt://%s\n", s_mqtt_address);

  //
  // Websocket server
  //
  struct mg_connection *websocket_listener;
  websocket_listener = mg_bind(&mgr, s_websocket_address, custom_websocket_handler);
  if (websocket_listener == NULL) {
    fprintf(stderr, "websocket mg_bind(%s) failed\n", s_websocket_address);
    exit(EXIT_FAILURE);
  }
  mg_set_protocol_http_websocket(websocket_listener);
  printf("websocket server started on ws://%s\n", s_websocket_address);

  /*
   * TODO: Add a HTTP status page that shows current sessions
   * and subscriptions
   */

  for (;;) {
    mg_mgr_poll(&mgr, 1000);
  }
}
