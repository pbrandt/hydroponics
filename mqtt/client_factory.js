var mqtt = require('async-mqtt')

/**
 * create an mqtt client
 * @param  {Object} config {verbose: false, host: 'localhost'}
 * @return {Object}        client with publish and subscribe
 */
module.exports = async function clientFactory(config) {
  if (config.verbose || config.debug || process.env.DEBUG) {
    debug = console.log.bind(console)
    debug('verbose logging enabled')
  } else {
    var debug = function () {}
  }

  if (!config.clientId) {
    throw new Error('Need to supply a clientId, like "sous-vide"')
  }

  debug('setting up mqtt client:', config)

  // hash of subscriptions and functions to run when you get a message on a sub
  var subscriptions = {}

  // parses wildcards
  function findSubscriptions(topic) {
    var subs = [];
    Object.keys(subscriptions).map(s => {
      var regex = new RegExp(s.replace('+', '.*'))
      if (topic.match(regex)) {
        subs = subs.concat(subscriptions[s])
      }
    })

    return subs;
  }

  // the mqtt client which will be set up later
  var client;

  /**
   * subscribe to a topic
   * @param  {string}   topic    topic like username/home/temperature
   * @param  {Function} callback function to be executed when message received on topic
   * @return {Promise}           [description]
   */
  async function subscribe (topic, callback) {
    debug('subscribing to', topic)

    if (!subscriptions[topic]) {
      subscriptions[topic] = []
      await client.subscribe(topic)
    }
    subscriptions[topic].push(callback)
  }

  /**
   * publish a message to a topic
   * @param  {string}  topic    topic like username/home/temperature
   * @param  {Any}     data     data to publish
   * @return {Promise}       [description]
   */
  async function publish (topic, data, options) {
    debug('publishing to', topic)
    client.publish(topic, JSON.stringify(data), options)
  }

  // connect to mqtt
  var windmyllClient = await new Promise((resolve, reject) => {
    debug('running mqtt.connect')
    client = mqtt.connect(config.host || 'mqtt://localhost:1883', {
      // username: config.id,
      // password: config.token,
      clientId: config.clientId,
      will: {
        topic: config.clientId + '/tel/online',
        payload: '0',
        qos: 1,
        retain: true
      }
    })

    client.on('connect', async () => {
      debug('connected', arguments)
      await publish(config.clientId + '/tel/online', 1, {
        qos: 1,
        retain: true
      })
      resolve({
        publish: publish,
        subscribe: subscribe
      })
    })

    client.on('error', e => {
      debug('error', e)
      reject(e)
    })

    client.on('message', (topic, message) => {
      debug('message on topic', topic)
      var subs = findSubscriptions(topic).map(s => {
        s(JSON.parse(message.toString()));
      })
      if (subs.length == 0) {
        throw new Error('No subscription for topic ' + topic)
      }
    })
  })

  return windmyllClient
}
